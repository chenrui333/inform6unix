Version 6.41 Release 2 (21 November 2022)
=========================================

* Updated PunyInform to version 4.0 commit 8a43434
* Hopefully better job of keeping Git stuff out of the distribution package.


Version 6.41 Release 1 (30 July 2022)
=====================================

* Inform 6 version 6.41 is finalized with commit f57b684
* Prevent accidentally packaging the wrong submodules when doing "make dist".
* Edited manpage to reflect changes for Inform 6.40 and 6.41.


Version 6.40 Release 1 (15 July 2022)
=====================================

* Inform 6 version 6.40 is finalized with commit 3300370
* Fixed portability problem in punyinform.sh script.


Version 6.36 Release 4 (30 May 2022)
====================================

* Updated PunyInform to version 4.0 commit e696f6d


Version 6.36 Release 3 (29 April 2022)
======================================

* Updated PunyInform to version 3.6 commit 7e1169e


Version 6.36 Release 2 (18 March 2022)
======================================

* Fixed a (recurring?) problem of needing a Gitlab login to get submodules.
  "make submodules" would fail on std/.


Version 6.36 Release 1 (18 February 2022)
=========================================

* Inform 6 version 6.36 is finalized with commit 5b515d5
* Updated Standard Library to version 6.12.6 commit 1339d85
* Updated PunyInform to version 3.5 commit 27aa648
* Updated pblorb.pl and scanblorb.pl from blorbtools commit f823ac7


Version 6.35 Release 8 (2 January 2022)
=======================================

* Updated PunyInform to version 3.4 commit dfee007


Version 6.35 Release 7 (21 December 2021)
=========================================

* Updated PunyInform to version 3.3 commit 4b479e1


Version 6.35 Release 6 (12 November 2021)
=========================================

* Updated PunyInform to version 3.2 commit 6640099


Version 6.35 Release 5 (23 October 2021)
========================================

* Updated PunyInform to version 3.1 commit 0d099e8


Version 6.35 Release 4 (8 October 2021)
=======================================

* Rolled back Inform 6 compiler prerelease 6.36 to 6.35.  A different
  package will contain that one from now on.

* Updated PunyInform to version 3.0 commit eba701c4f1.

* Updated package to allow for multiple versions of the Inform 6
  compiler to be present on the same machine.


Version 6.35 Release 3 (26 August 2021)
=======================================

* Removed dependency on GNU cut's --complement flag for configuring
  punyinform.sh.

* Averted trouble if foo.z3 and foo.z5 both exist in the same subdirectory.

* Use REAL_PREFIX to ensure punyinform.sh is set up right when fakeroot
  is used.  This is important when building binary packages (.deb, .rpm,
  and so on).

* Updated PunyInform to version 2.8.

* Updated Inform 6 compiler to 6.35 commit 09b7bc26d9


Version 6.35 Release 2 (21 June 2021)
=====================================

* Cleaned up error-prone dist target.

* Tweaks and clarification to README.md.

* Pulled in latest of PunyInform to fix warnings, removed the text
  "revealing..." when opening a container from the inside, edited
  grammar for SHOUT and JUMP.


Version 6.35 Release 1 (18 June 2021)
=====================================

* Updated Compiler to 6.35.

* Updated Standard Library to 6.12.5.

* Added PunyInform and reworked installation processes to allow multiple
  libraries to coexist.

* Assorted compatibility tweaks.

* Removed Library version from package version because there are now two
  libraries in the package.  From here on, the release number will
  simply be incremented when a library is updated or some other tweaks
  are necessary.  The version number will always be that of the
  compiler.


Version 6.34-6.12.4 Release 1 (2 August 2020)
=============================================

* Updated Compiler to latest 6.34.  This will work with PunyInform.

* Updated Library to 6.12.4.

* Reworked Makefile to get along better with macOS.

* Added a Release number to indicate updates to the package but not to
  the compiler or library.


Version 6.33-6.12.2 (20 May 2018)
=================================

* The Inform Library is 6.12.1 with lots of bugfixes and a couple minor 
  enhancements.


Version 6.33-6.12.1 (6 June 2016)
=================================

* Inform version is now 6.33, with Inform7-related patches and new features.

* The Inform Library is 6.12.1 with lots of bugfixes and enhancements.

* Package version scheme changed to indicate both compiler and library 
  versions included.

* Include files trimmed to those known to be freely redistributable and
  checked to make sure they work.

* DM4 removed due to license incompatibilities.

* Added a manpage.

* Added pblorb.pl and scanblorb.pl utilities for dealing with Blorb files.


Version 6.32.1 (16 July 2012)
=============================

* Inform version is now 6.32, with more patches for use with Inform 7.

* The Inform program is now distributed under the Artistic License 2.0.

* The advent.inf example is now at release 9.


Version 6.31.1 (19 May 2006)
============================

* New version of compiler which fixes several bugs found in the development
  of Inform 7.

* Package now uses the latest autotools.


Version 6.30.2 (6 Jun 2004)
===========================

* The HTML version of the Inform Designer's Manual is now the Fourth
  Edition.

* The Info version of the Designer's Manual has been discontinued, since it
  refers to the out-of-date Third Edition.

* Many more contributed include files from the IF archive are now present
  in the package.  Some of them come with documentation files, which are
  also installed (in the same include directory).

* Applied long path length fix to all unices (e.g. FreeBSD), not just
  Linux.

* Minor package fixes.


Version 6.30.1 (27 Feb 2004)
============================

* New version of biplatform compiler.

* Included DM4 misprints; removed patch directory and bug notice.

* Long path length is now standard for Linux.


Version 6.21.4 (17 Jun 2003)
============================

* Applied patches from www.inform-fiction.org to the source and library, as
  supplied by Dave Griffith <dgriffi@cs.csubak.edu>.

* There's a new Ruins demo game, with more stuff implemented.  The previous
  Ruins demos have been renamed: ruins -> ruins1, ruinsplus -> ruins2.  The
  new one is called ruins3.

* The Alice demo in the 'tutor' directory has been updated to the latest
  Inform version.

* Included a more up-to-date inform-mode.el.


Version 6.21.3 (6 Nov 2002)
===========================

* The Inform included here is now Andrew Plotkin's bi-platform version,
  which can compile for Glulx, following a suggestion by Marshall
  T. Vandegrift <vandem2.rpi.edu>.

* Now includes HTML version of the Designer's Manual.

* Several more library include files have been added.


Version 6.21.2 (15 Aug 2000)
============================

* Minor installation changes for OpenBSD, as suggested by David Leonard
  <david.leonard@csee.uq.edu.au>.


Version 6.21.1 (24 Nov 1999)
============================

* Initial release.
